from pathlib import Path

from utils import visualizer, path
from utils.data_generator import create_data_generators
from stages.evaluate import evaluate

def cross_validate(iteration_dir, iteration, model, P_X, P_y):
    n_patients = len(P_X)
    cv_results = {}

    for leave_out_i in range(n_patients):
        print(f'Cross Validation: Leave out patient {leave_out_i+1}')
        print('*****************************')

        leave_out_dir = Path(iteration_dir, str(f'leave_out_{leave_out_i+1}'))
        path.clean_or_create_dir(leave_out_dir)

        print('Create data generator.')
        train_generator, test_generator, class_w = create_data_generators(P_X, P_y, leave_out_i, seq_len=1, fs=125)

        model = iteration['model']
        if iteration['load_model_from_file']:
            print('Loading model from file.')
            model.load(leave_out_dir)
        else:
            print('Fitting model.')

            fit_dir = Path(leave_out_dir,'fit_history')
            path.clean_or_create_dir(fit_dir)

            fit_history_save_path = Path(leave_out_dir, 'fit_history.png')
            model.fit(train_generator, class_weight=class_w, validation_data=test_generator, **iteration['fit'], title='Fit History, Leave Out Patient ' + str(leave_out_i))

            print('Saving model.')
            model.save(leave_out_dir)

            # visualizer.show()

        print('Evaluating model.')

        print('Evaluating Patient Number {}'.format(leave_out_i+1))
        patient_results_dir = Path(leave_out_dir, 'results_patient_'+str(leave_out_i+1))
        path.clean_or_create_dir(patient_results_dir)

        run_results = evaluate(model, P_X[leave_out_i], P_y[leave_out_i], save_dir=patient_results_dir)
        cv_results['leave patient ' + str(leave_out_i) + ' out'] = run_results

        # visualizer.show()

        print('Resetting model.')
        model.reset()

    return cv_results
