import numpy as np
import os
import h5py
from pathlib import Path
import json

"If you are trying to get the model to run with your own data, you will need to make a new loader class here, and use it in the 'iteration' dictionary in main. Two examples are provided below. The format which main requires is two lists of records, the first, P_X, the second, P_y. The records in P_X (X) have the EEG signal for a patient recording, and have shape (#samples, #channels). The records in P_y (y) have the epoch labels for the recordings, and have shape (#labels)"

"Loader for the 12 patient PSG and Cognionics sleep study."
class CognionicsPsgStudyLoader():
    def __init__(self, **kwargs):
        default_params = {
            'device': 'HB',
            'dataset_path': '../data/dataset.jld',
        }
        self.params = default_params
        for param, value in kwargs.items():
            self.params[param] = value

    "Reads dataset and returns P_X, and P_y (list of X records, and list of y records). X is the EEG signal, and has shape (#samples, #channels). y is the epoch labels, and has shape (#labels)"
    def read_dataset(self):
        dataset = h5py.File(self.params['dataset_path'], 'r')
        device = self.params['device']
        n_patients = 12
        P_X, P_y = {}, {}
        for i in range(n_patients):
            raw_eeg = np.array(dataset['data'][str(i+1)][device]['EEG'])
            raw_eeg = raw_eeg[0:len(raw_eeg)-1,:]
            P_X[i] = raw_eeg
            P_y[i] = np.array(np.transpose(dataset['labels'][str(i+1)]))

        return list(P_X.values()), list(P_y.values())

"Loader for the dreem headband open study, the data can be found at this link:"
"https://dreem-octave-irba.s3.eu-west-3.amazonaws.com/index.html#h01/"
class DreemHeadbandLoader():
    def __init__(self, **kwargs):
        default_params = {
            'dataset_dir': '../data/dreem',
            # the dataset contains a 'raw' signal and a 'visualization' signal
            'use_visualization_signal': False,
            'records': range(1, 3), # [L, R) and starts from 1
            'expert_scorer_to_use': 1, # there are 5 scorers
            'channels': [1, 2]
        }
        self.params = default_params;

        for param, value in kwargs.items():
            self.params[param] = value

    "Reads dataset and returns P_X, and P_y (list of X records, and list of y records). X is the EEG signal, and has shape (#samples, #channels). y is the epoch labels, and has shape (#labels)"
    def read_dataset(self):
        P_X, P_y = {}, {}

        signal = 'raw'
        if self.params['use_visualization_signal']:
            signal = 'visualization'

        dreem_eeg_freq = 250
        epoch_time_length = 30

        hypnogram_path = Path(self.params['dataset_dir'], 'hypnograms_scorers.json')
        hypnogram = json.load(hypnogram_path.open())

        hypnogram_start_times_path = Path(self.params['dataset_dir'], 'start_times_hypnograms.json')
        hypnogram_start_times = json.load(hypnogram_start_times_path.open())

        scorer = "Scorer "+str(self.params['expert_scorer_to_use'])

        for record_n in self.params['records']:
            record_id = 'h'+'{:02}'.format(record_n)

            P_y[record_n] = hypnogram[record_id][scorer]

            record_path = Path(self.params['dataset_dir'], record_id, 'dreem.h5')
            record = h5py.File(record_path, 'r')

            capture_start_time = record.attrs['start_time']
            hypnogram_start_time = hypnogram_start_times[record_id]
            record_start_time_offset = hypnogram_start_time - capture_start_time
            record_start_offset = record_start_time_offset * dreem_eeg_freq

            record_time_length = len(P_y[record_n]) * epoch_time_length
            record_length = record_time_length * dreem_eeg_freq

            X = np.empty((0, 0))
            for i in self.params['channels']:
                X_channel = np.array(record['channel'+str(i)][signal])
                X_channel = X_channel[record_start_offset:(record_start_offset+record_length)]
                X_channel = X_channel[..., np.newaxis]
                if not X.any():
                    X = X_channel
                else:
                    X = np.concatenate((X, X_channel), axis=1)

            P_X[record_n] = X

        return list(P_X.values()), list(P_y.values())
