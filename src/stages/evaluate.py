from sklearn.metrics import f1_score, accuracy_score, confusion_matrix, log_loss, balanced_accuracy_score
import os, sys
from utils import visualizer
from keras.utils import to_categorical
from pprint import pprint
from keras.utils import plot_model
import csv

def evaluate(model, X_test, y_test, save_dir=''):
    if save_dir != '':
        os.makedirs(save_dir, exist_ok=True)

    y_pred = model.predict(X_test)

    results = {}

    accuracy = accuracy_score(y_test, y_pred)
    print('\tAccuracy: {}'.format(accuracy))
    results['Accuracy'] = accuracy

    balanced_accuracy = balanced_accuracy_score(y_test, y_pred)
    print('\tBalanced Accuracy: {}'.format(balanced_accuracy))
    results['Balanced Accuracy'] = balanced_accuracy

    f1 = f1_score(y_test, y_pred, average='weighted')
    print('\tF1 Score: {}'.format(f1))
    results['F1 Score'] = f1

    conf_matrix = confusion_matrix(y_test, y_pred)
    print('\tConfusion Matrix: \n{}'.format(conf_matrix))
    if save_dir != '':
        visualizer.plot_confusion_matrix(conf_matrix,['W', 'S1', 'S2', 'S3', 'REM'], cbar=False, save_path=os.path.join(save_dir, 'confusion_matrix.png'))
    else:
        visualizer.plot_confusion_matrix(conf_matrix,['W', 'S1', 'S2', 'S3', 'REM'], cbar=False)
    results['Confusion Matrix'] = conf_matrix

    print('\tHypnogram: \n')
    visualizer.plot_prediction_hypnogram(y_test, y_pred, save_path=os.path.join(save_dir, 'prediction_hypnogram.png'))

    print('\tShowing Mislabeled Epochs')
    visualizer.plot_mislabled_epochs(X_test, y_pred, y_test)

    if save_dir != '':
        with open(os.path.join(save_dir, 'results.csv'), 'w') as f:
            for key in results.keys():
                f.write("%s, %s\n" % (key, results[key]))

    return results
