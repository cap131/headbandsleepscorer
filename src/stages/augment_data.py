import tensorflow as tf
from pathlib import Path
import json
import pickle
import scipy.io as sio
from imblearn.over_sampling import SMOTE
from os import listdir
import h5py
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal as sp
import matplotlib
import matplotlib.pyplot as plt
from scipy.io import loadmat
from utils.signals import partition_into_epochs
from scipy.signal import firwin, convolve

class DataAugmenter:
    flush_cache = False
    params = {}

    def augment(self, P_X, P_y):
        augmenter_class_name = type(self).__name__
        print(f'Using data augmenter: {augmenter_class_name}')

        cache_dir = Path('../data', augmenter_class_name)
        if not cache_dir.exists():
            cache_dir.mkdir()

        saved_params_file = Path(cache_dir, 'saved_data_params.json')
        saved_dataset_file = Path(cache_dir, 'cached_dataset.p')

        if Path(saved_params_file).is_file():
            saved_params = json.load(saved_params_file.open())
            if not self.flush_cache and saved_params == self.params and Path(saved_dataset_file).is_file():
                print('Using cached augmented dataset.')
                return pickle.load(saved_dataset_file.open(mode='rb'))

        print('Generating augmented dataset.')

        P_X_augmented, P_y_augmented = P_X, P_y
        for i in range(len(P_X)):
            print(f'Augmenting patient: {i}')
            P_X_augmented[i], P_y_augmented[i] = self.augment_batch(P_X[i], P_y[i])

        dataset = (P_X_augmented, P_y_augmented)
        json.dump(self.params, open(saved_params_file, 'w'))
        pickle.dump(dataset, open(saved_dataset_file, 'wb'))

        return P_X_augmented, P_y_augmented

    def augment_batch(X, y):
        return X, y

class FilterUncorrelated(DataAugmenter):
    def __init__(self, **kwargs):
        self.params = {
            'min_avg_epoch_correlation': 0.8,
            'min_channel_amp': 40,
        }

        for param, value in kwargs.items():
            if param == 'flush_cache':
                self.flush_cache = value
            else:
                self.params[param] = value


    def augment_batch(self, X, y):
        chan1 = X[:,:,0]
        chan2 = X[:,:,1]

        new_chan1 = []
        new_chan2 = []
        new_labels = []

        for epoch_i in range(chan1.shape[0]):
            corr = np.corrcoef(chan1[epoch_i,:], chan2[epoch_i,:])
            avg_corr = np.mean(np.fliplr(corr).diagonal())
            # all_corrs.append(avg_corr)

            avg_amp_chan1 = np.mean(np.abs(chan1[epoch_i,:]))
            avg_amp_chan2 = np.mean(np.abs(chan2[epoch_i,:]))

            if(avg_corr > self.params['min_avg_epoch_correlation']):
                new_chan1.append(chan1[epoch_i,:])
                new_chan2.append(chan2[epoch_i,:])
                new_labels.append(y[epoch_i])
            elif(avg_amp_chan1 < self.params['min_channel_amp'] and avg_amp_chan2 < self.params['min_channel_amp']):
                new_chan1.append(chan1[epoch_i,:])
                new_chan2.append(chan2[epoch_i,:])
                new_labels.append(y[epoch_i])
            else:
                print(f'Filter Uncorrelated: Removing {epoch_i} - correlation is {avg_corr}, and (L, R) channel average amplitude is ({avg_amp_chan1}, {avg_amp_chan2})')

        np_chan1 = np.array(new_chan1)
        np_chan1 = np_chan1[..., np.newaxis]
        np_chan2 = np.array(new_chan2)
        np_chan2 = np_chan2[..., np.newaxis]

        X_filtered = np.concatenate((np_chan1, np_chan2), axis=2)
        y_filtered = np.array(new_labels)

        return X_filtered, y_filtered

class OverlappingWindows(DataAugmenter):
    def __init__(self, **kwargs):
        self.params = {
            'percent_overlap': 0.5,
        }

        for param, value in kwargs.items():
            if param == 'flush_cache' and value == value:
                self.flush_cache = value
            else:
                self.params[param] = value

    def __get_overlapping_windows(self, arr, w, o):
        sh = (arr.size - w + 1, w)
        st = arr.strides * 2
        view = np.lib.stride_tricks.as_strided(arr, strides = st, shape = sh)[0::o]
        return view

    def augment_batch(self, X, y):
        percent_overlap = self.params['percent_overlap']
        samples_per_epoch = len(X[0])

        # find number of contiguous epochs with same class using hypnogram
        hypnogram_blocks = [];
        block_length = 1;
        prev_class = y[0];
        block = []
        for i in range(1,len(y)):
            if(y[i]!=prev_class):
                block.append(block_length)
                block.append(prev_class)
                hypnogram_blocks.append(block)
                block = []
                block_length = 0
            block_length +=1
            if(i==(len(y) -1) and (y[i]!=prev_class)):
                block.append(block_length)
                block.append(y[i])
                hypnogram_blocks.append(block)
            elif(i==(len(y) -1)):
                block = []
                block.append(block_length)
                block.append(y[i])
                hypnogram_blocks.append(block)
                break
            prev_class = y[i]

        # divide the epochs into contiguous blocks of same class
        epoch_index_offset = 0
        contiguous_blocks_f3m2 = []
        contiguous_blocks_f4m1 = []
        ch_f3m2 = 0
        ch_f4m1 = 1
        for block_num in range(0, len(hypnogram_blocks)):
            block_f3m2 = []
            block_f4m1 = []
            epochs_per_block = hypnogram_blocks[block_num][0]
            for epoch_num in range(0,epochs_per_block):
                for sample_num in range(0, samples_per_epoch):
                    block_f3m2.append(X[epoch_num+epoch_index_offset][sample_num][ch_f3m2])
                    block_f4m1.append(X[epoch_num+epoch_index_offset][sample_num][ch_f4m1])
            contiguous_blocks_f3m2.append(block_f3m2)
            contiguous_blocks_f4m1.append(block_f4m1)

            epoch_index_offset += epochs_per_block

        # perform overlapping windows on each block
        overlapping_f3m2 = []
        overlapping_f4m1 = []
        for block in range(0,len(hypnogram_blocks)):
            overlapping_f3m2.append(self.__get_overlapping_windows(np.array(contiguous_blocks_f3m2[block]),
                                                                   samples_per_epoch,
                                                                   int(samples_per_epoch * (1-percent_overlap))))
            overlapping_f4m1.append(self.__get_overlapping_windows(np.array(contiguous_blocks_f4m1[block]),
                                                                   samples_per_epoch,
                                                                   int(samples_per_epoch * (1-percent_overlap))))

        # reformat the data
        hypnogram_overlap = []
        data_overlap = []
        for b in range(0,len(hypnogram_blocks)):

            label = hypnogram_blocks[b][1]
            num_epochs_per_block = len(overlapping_f3m2[b])
            hypnogram_overlap.append(num_epochs_per_block*[label])

            for epoch_num in range(0,num_epochs_per_block):
                curr_epoch = []
                for sample_num in range(0, samples_per_epoch):
                    sample = []
                    sample.append(overlapping_f3m2[b][epoch_num][sample_num])
                    sample.append(overlapping_f4m1[b][epoch_num][sample_num])
                    curr_epoch.append(sample)
                data_overlap.append(curr_epoch)

        X_overlap = np.array(data_overlap)
        y_overlap = np.array([item for sublist in hypnogram_overlap for item in sublist])

        return X_overlap,y_overlap


class Downsample(DataAugmenter):
    def __init__(self, **kwargs):
        self.params = {
            'decim': 5,
        }

        for param, value in kwargs.items():
            if param == 'flush_cache' and value == value:
                self.flush_cache = value
            else:
                self.params[param] = value

    def augment_batch(self, X, y):
        decim = self.params['decim']
        samples_per_epoch = len(X[0])
        num_epochs = len(X)
        fs = int(samples_per_epoch/30)
        signal_f3m2 = []
        ch_f3m2 = 0
        signal_f4m1 = []
        ch_f4m1 = 1

        # apply low pass filter to avoid aliasing
        for epoch in range(0, num_epochs):
            for sample in range(0, samples_per_epoch):
                    signal_f3m2.append(X[epoch][sample][ch_f3m2])
                    signal_f4m1.append(X[epoch][sample][ch_f4m1])
        lowpass_freq = 1/(2 * decim)
        taps = int(2 * 0.2 * fs) # this gives a latency of 200 ms
        b = firwin(taps, cutoff = lowpass_freq, fs=fs) # create a FIR filter
        f3m2_filtered = convolve(signal_f3m2, b, mode='same')
        f4m1_filtered = convolve(signal_f4m1, b, mode='same')
        X_filtered = X.copy()
        epoch_offset = 0
        for epoch in range(0, num_epochs):
            for sample in range(0, num_samples):
                X_filtered[epoch][sample][ch_f3m2] = f3m2_filtered[sample + epoch_offset]
                X_filtered[epoch][sample][ch_f4m1] = f4m1_filtered[sample + epoch_offset]
            epoch_index += samples_per_epoch


        # get decim number of downsample versions of the data
        X_list = X_filtered.copy().tolist()
        downsample_versions = []
        offset = 0
        for version in range(0,decim):
            epochs = []
            for epoch_num in range(0,num_epochs):
                epochs.append(X_list[epoch_num][offset::decim])
            offset +=1
            downsample_versions.append(epochs)

        # concatenate all the versions together
        downsamples = []
        hyp = y.copy().tolist()
        hypnogram_downsample = []
        for version in range(0,decim):
            downsamples = downsamples + downsample_versions[version]
            hypnogram_downsample = hypnogram_downsample + hyp

        X_downsample = np.array(downsamples)
        y_downsample = np.array(hypnogram_downsample)

        return X_downsample, y_downsample

class GaussianNoiseAddition(DataAugmenter):
    def __init__(self, **kwargs):
        self.params = {
            'mean': 0,
            'standard_deviation': 0.1,
        }

        for param, value in kwargs.items():
            if param == 'flush_cache' and value == value:
                self.flush_cache = value
            else:
                self.params[param] = value
    def augment_batch(self, X, y):
        mean = self.params['mean']
        standard_deviation = self.params['standard_deviation']

        num_samples = (samples_per_epoch * num_epochs)
        samples_per_epoch = len(X[0])
        num_epochs = len(X)

        # create gaussian noise
        noise = np.random.normal(mean,standard_dev,num_samples)

        X_gaussian = X.copy()
        y_gaussian = y.copy()
        sample_offset = 0
        for epoch in range(0, num_epochs):
            for sample in range(0, samples_per_epoch):
                X_gaussian[epoch][sample][f3m2_ch_num] += noise[sample + sample_offset]
                X_gaussian[epoch][sample][f4m1_ch_num] += noise[sample + sample_offset]
            sample_offset += samples_per_epoch

        return X_gaussian, y_gaussian


class SmoteOversampler(DataAugmenter):
    def __init__(self, **kwargs):
        self.params = {
            'fs': 100,
            'epoch_length': 30,
        }

        for param, value in kwargs.items():
            if param == 'flush_cache' and value == True:
                self.flush_cache = True
            else:
                self.params[param] = value

    def augment_batch(self, X, y):
        fs = self.params['fs']
        epoch_len = self.params['epoch_length']

        num_epochs = len(X)
        samples_per_epoch = fs*epoch_len
        ch_num_f3m2 = 0
        ch_num_f4m1 = 1

        X_train_F3M2 = []
        X_train_F4M1 = []
        sample_f3m2 = []
        sample_f4m1 = []
        time = 0
        delta_t = 1/fs

        for i in range(0, num_epochs):
            curr_epoch = X[i]
            for j in range(0,samples_per_epoch):
                sample_f3m2.append(time)
                sample_f4m1.append(time)
                time += delta_t
                sample_f3m2.append(curr_epoch[j][ch_num_f3m2])
                sample_f4m1.append(curr_epoch[j][ch_num_f4m1])
                X_train_F3M2.append(sample_f3m2)
                X_train_F4M1.append(sample_f4m1)
                sample_f3m2 = []
                sample_f4m1 = []

        y_train = []
        labels_curr_epoch = []
        sleep_stage = 0

        for h in range(0,num_epochs):
            sleep_stage = y[h]
            labels_curr_epoch = [sleep_stage]*(samples_per_epoch)
            y_train.extend(labels_curr_epoch)

        y_train_f3m2 = y_train
        y_train_f4m1 = y_train

        oversample_f3m2 = SMOTE(sampling_strategy='all')
        X_train_F3M2, y_train_f3m2 = oversample_f3m2.fit_resample(X_train_F3M2, y_train_f3m2)
        oversample_f4m1 = SMOTE(sampling_strategy='all')
        X_train_F4M1, y_train_f4m1 = oversample_f4m1.fit_resample(X_train_F4M1, y_train_f4m1)

        chan_1_aug = []
        chan_2_aug = []
        y_aug = []
        hyp_index = 0
        oversample_num_samples = len(X_train_F3M2)

        for m in range(0,oversample_num_samples):
            if(hyp_index < oversample_num_samples):
                y_aug.append(y_train_f3m2[hyp_index])
                hyp_index += (samples_per_epoch)
            chan_1_aug.append(X_train_F3M2[m][1])
            chan_2_aug.append(X_train_F4M1[m][1])

        np_chan_1_aug = np.array(chan_1_aug)
        np_chan_1_aug = partition_into_epochs(np_chan_1_aug, fs*epoch_len)
        np_chan_1_aug = np_chan_1_aug[..., np.newaxis]

        np_chan_2_aug = np.array(chan_2_aug)
        np_chan_2_aug = partition_into_epochs(np_chan_2_aug, fs*epoch_len)
        np_chan_2_aug = np_chan_2_aug[..., np.newaxis]

        X_aug = np.concatenate((np_chan_1_aug, np_chan_2_aug), axis=2)
        y_aug = y_train_f3m2

        return X_aug, y_aug

class Sequence(DataAugmenter):
    def __init__(self, sequence, **kwargs):
        self.sequence = sequence
        self.flush_cache = False

        for param, value in kwargs.items():
            self.params[param] = value

    def augment(self, P_X, P_y):
        P_X_augmented, P_y_augmented = P_X, P_y
        for augmenter in self.sequence:
            print(f'\nStarting Augmentation with: {type(augmenter).__name__}')
            if self.flush_cache:
                augmenter.flush_cache = True
            elif augmenter.flush_cache:
                self.flush_cache = True

            P_X_augmented, P_y_augmented = augmenter.augment(P_X_augmented, P_y_augmented)

        return P_X_augmented, P_y_augmented
