import numpy as np
import os
import h5py
from utils import visualizer
from utils.signals import fir_bandpass_filter, partition_into_epochs
from scipy.signal import decimate

import pickle
from pathlib import Path
import json

class DataPreprocesser:
    flush_cache = False
    params = {
        'device': 'HB',
        'original_sample_rate': 500,
        'dataset_path': '../data/dataset.jld',
    }

    def preprocess(self, P_X, P_y):
        preprocesser_class_name = type(self).__name__
        print(f'Using data preprocesser: {preprocesser_class_name}')

        cache_dir = Path('../data', preprocesser_class_name)
        if not cache_dir.exists():
            cache_dir.mkdir()

        saved_params_file = Path(cache_dir, 'saved_data_params.json')
        saved_dataset_file = Path(cache_dir, 'cached_dataset.p')

        if Path(saved_params_file).is_file():
            saved_params = json.load(saved_params_file.open())
            if self.params and not self.flush_cache and saved_params == self.params and Path(saved_dataset_file).is_file():
                print('Using cached preprocessed dataset.')
                return pickle.load(saved_dataset_file.open(mode='rb'))

        print('Generating preprocessed dataset.')
        dataset = h5py.File(self.params['dataset_path'], 'r')

        for i in range(len(P_X)):
            P_X[i], P_y[i] = self.preprocess_batch(P_X[i], P_y[i], patient_n=i+1)

        json.dump(self.params, open(saved_params_file, 'w'))
        dataset = (P_X, P_y)
        pickle.dump(dataset, open(saved_dataset_file, 'wb'))

        return P_X, P_y

    def preprocess_batch(X, y):
        return X, y

class GenericPreprocesser(DataPreprocesser):
    def __init__(self, **kwargs):
        self.params.update({
            'epoch_length': 30,
            'downsample_factor': 1,
            'highpass_f': 0.5,
            'visualize': False
        })

        for param, value in kwargs.items():
            if param == 'flush_cache':
                self.flush_cache = value
            else:
                self.params[param] = value

    def preprocess_channel(self, samples, patient_n=1, channel_n=1):
        print('Downsampling signal.')
        samples_downsampled = decimate(samples, self.params['downsample_factor'], ftype='fir')

        print('Filtering signal.')
        new_sample_rate = self.params['original_sample_rate'] / self.params['downsample_factor']
        samples_filtered = fir_bandpass_filter(samples_downsampled, self.params['highpass_f'], new_sample_rate)

        print('Partitioning signal into epochs.')
        epoch_sample_length = self.params['epoch_length'] * self.params['original_sample_rate'] / self.params['downsample_factor']
        channel_epochs = partition_into_epochs(samples_filtered, epoch_sample_length)

        if self.params['visualize']:
            channel_epochs_before = partition_into_epochs(samples_downsampled, epoch_sample_length)
            visualizer.plot_epochs(channel_epochs_before, data2=channel_epochs, title=str(f'Preprocess Patient {patient_n}, Channel: {channel_n}'), data1_label='Before', data2_label='After')
            visualizer.show()

        return channel_epochs

    def preprocess_batch(self, X, y, patient_n=1):
        assert(self.params['original_sample_rate'] % self.params['downsample_factor'] == 0)

        print(f'Preprocessing Channel 1')
        ch_1 = self.preprocess_channel(X[:,0], patient_n=patient_n, channel_n=1)
        ch_1 = ch_1[..., np.newaxis]

        print(f'Preprocessing Channel 2')
        ch_2 = self.preprocess_channel(X[:,1], patient_n=patient_n, channel_n=2)
        ch_2 = ch_2[..., np.newaxis]

        X_preprocessed = np.concatenate((ch_1, ch_2), axis=2)

        return (X_preprocessed, y)
