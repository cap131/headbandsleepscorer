import os
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, BatchNormalization, Activation
from keras.layers import LSTM, Reshape,Permute, TimeDistributed, Bidirectional
from keras.layers import MaxPooling2D, Conv2D, Conv1D, MaxPooling1D
from keras.optimizers import Adadelta, RMSprop, Adam
from keras import layers
from keras import models
import numpy as np

from models.keras_model import KerasModel

def cnn3(input_shape=(3000, 2), filters=[128, 256, 300], kernel_size = [(50), (5), (5)], n_classes = 5, iteration = 0, load_path=None):
    model = Sequential(name='cnn3')

    model.add(Conv1D (kernel_size = kernel_size[0], filters = filters[0], strides=5, input_shape=input_shape, kernel_initializer='he_normal', activation='relu',kernel_regularizer=keras.regularizers.l2(0.05)))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(Conv1D (kernel_size = kernel_size[1], filters = filters[1], strides=1, kernel_initializer='he_normal', activation='relu',kernel_regularizer=keras.regularizers.l2(0.01)))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(MaxPooling1D())
    model.add(Conv1D (kernel_size = kernel_size[2], filters = filters[2], strides=2, kernel_initializer='he_normal', activation='relu',kernel_regularizer=keras.regularizers.l2(0.01)))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(MaxPooling1D())
    model.add(Flatten(name='conv3'))
    model.add(Dense (1500, activation='relu', kernel_initializer='he_normal',name='fc1'))
    model.add(BatchNormalization(name='bn1'))
    model.add(Dropout(0.5, name='do1'))
    model.add(Dense (1500, activation='relu', kernel_initializer='he_normal',name='fc2'))
    model.add(BatchNormalization(name='bn2'))
    model.add(Dropout(0.5, name='do2'))
    model.add(Dense(n_classes, activation = 'softmax',name='softmax'))

    return KerasModel(model)
