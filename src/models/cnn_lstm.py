import os
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, BatchNormalization, Activation, Input
from keras.layers import LSTM, Reshape,Permute, TimeDistributed, Bidirectional
from keras.layers import MaxPooling2D, Conv2D, Conv1D, MaxPooling1D
from keras.optimizers import Adadelta, RMSprop, Adam
from keras import layers
from keras import models, regularizers
import numpy as np

from models.keras_model import KerasModel

class CnnLstm(KerasModel):
    def __init__(self, input_shape=(3000,2), filters=[8, 16, 32], kernel_size = [8, 8, 8], lstm_units=[64, 64]):
        self.keras_model = models.Sequential()
        self.keras_model.add(Input(shape=input_shape))
        self.keras_model.add(Conv1D(filters[0], kernel_size=kernel_size[0], input_shape=input_shape))
        self.keras_model.add(Activation('relu'))
        self.keras_model.add(MaxPooling1D(pool_size=3,strides=(3)))
        self.keras_model.add(Conv1D(filters[1], kernel_size=kernel_size[1]))
        self.keras_model.add(Activation('relu'))
        self.keras_model.add(MaxPooling1D(pool_size=3,strides=(3)))
        self.keras_model.add(Conv1D(filters[2], kernel_size=kernel_size[2]))
        self.keras_model.add(Activation('relu'))
        self.keras_model.add(MaxPooling1D(pool_size=3,strides=(3)))
        self.keras_model.add(Permute((1,2)))

        l1l2 = regularizers.l1_l2(l1 = 0, l2 = 0.001)

        self.keras_model.add(LSTM(lstm_units[0], return_sequences=True, kernel_regularizer=l1l2, dropout=0.2))
        self.keras_model.add(LSTM(lstm_units[1]))
        self.keras_model.add(Dense(5, activation='softmax'))
