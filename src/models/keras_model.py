import os, sys
from utils import visualizer
from sklearn.model_selection import GroupKFold
from tensorflow.keras.models import load_model
from keras.utils import to_categorical
from keras.utils import plot_model
from sklearn.utils import class_weight
import numpy as np

class KerasModel:
    def __init__(self, keras_model):
        self.keras_model = keras_model

    def print(self):
        self.keras_model.summary()

    def compile(self, **args):
        self.empty_weights = self.keras_model.get_weights()
        self.keras_model.compile(**args)

    def reset(self):
        self.keras_model.set_weights(self.empty_weights)

    def load(self, load_dir):
        weights_dir = os.path.join(load_dir, 'weights')
        self.keras_model = load_model(weights_dir)

    def save(self, save_dir):
        try:
            weights_dir = os.path.join(save_dir, 'weights')
            self.keras_model.save(weights_dir)

            save_plot_path = os.path.join(save_dir, 'model.png')
            plot_model(self.keras_model, to_file=save_plot_path)
        except Exception as error:
            print("Got an error while saving model: {}".format(error))

    def fit(self, generator, save_path='', title='', **args):
        # TODO may not work with data generator
        history = self.keras_model.fit(generator, **args)
        if save_path != '':
            visualizer.plot_keras_model_fit_history(history, save_path=save_path, title='Fit History')


    def predict(self, X, **args):
        return self.keras_model.predict_classes(X, **args)
