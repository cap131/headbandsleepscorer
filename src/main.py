import numpy as np
from pathlib import Path
from sklearn.model_selection import GroupKFold
from keras.utils import to_categorical
from keras.optimizers import Adam

from utils import visualizer, path
from models.cnn_lstm import CnnLstm
from stages.load_data import CognionicsPsgStudyLoader
from stages.preprocess import GenericPreprocesser
from stages.augment_data import FilterUncorrelated, OverlappingWindows, Sequence
from stages.cross_validation import cross_validate
from stages.evaluate import evaluate

import tensorflow as tf
gpu_devices = tf.config.experimental.list_physical_devices('GPU')
for device in gpu_devices: tf.config.experimental.set_memory_growth(device, True)

iteration = {
    'iteration_name': 'final_cnn_lstm',
    'load_model_from_file': False,
    'notes': 'system used to create results in paper',
    'dataset_loader': CognionicsPsgStudyLoader(
        device='HB',
        dataset_path='../data/dataset.jld'
    ),
    'preprocesser': GenericPreprocesser(
        epoch_length=30,
        original_sample_rate=500,
        downsample_factor=20,
        bandpass_frequency_range = [0.50, 12],
        flush_cache=True,
        visualize=False
    ),
    'data_augmenter': Sequence([
        FilterUncorrelated(
            min_avg_epoch_correlation=0.9,
            min_channel_amp=40
        ),
        OverlappingWindows(
            percent_overlap = 0.75,
            classes_to_augment=[1,2,3,4,5]
        )
    ]),
    'model': CnnLstm(
        input_shape=(25*30, 2), # NOTE set this to ( downsample freq * epoch length, #channels)
        filters=[8, 16, 32],
        kernel_size = [8, 8, 8],
        lstm_units=[64, 64]
    ),
    'compile': {
        'optimizer': Adam(lr=0.00005),
        'loss': 'categorical_crossentropy',
        'metrics': ['accuracy']
    },
    'fit': {
        'shuffle': True,
        'epochs': 100,
        'workers': 6,
        'batch_size': 64
    }
}

iteration_dir = Path('iterations', iteration['iteration_name'])
if not iteration['load_model_from_file']:
    path.create_new_iteration_dir(iteration_dir, iteration)

print('Loading Dataset')
P_X, P_y = iteration['dataset_loader'].read_dataset()

print('Preprocessing data.')
P_X, P_y = iteration['preprocesser'].preprocess(P_X, P_y)

print('Augmenting data.')
P_X, P_y = iteration['data_augmenter'].augment(P_X, P_y)

print('Compiling model.')
model = iteration['model']
model.compile(**iteration['compile'])

print('*****************************')
print(f'Cross Validation Begin')
print('*****************************')
cv_results = cross_validate(iteration_dir, iteration, model, P_X, P_y)
n_patients = len(P_X)
path.write_results(iteration_dir, cv_results, n_patients)
