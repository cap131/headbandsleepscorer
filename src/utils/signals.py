import scipy.signal as signal
import numpy as np

def partition_into_epochs(samples, epoch_sample_size):
    n_epochs = int(len(samples) / epoch_sample_size)
    epochs = np.split(samples, n_epochs)
    return np.array(epochs)

def fir_bandpass_filter(samples, frequency_range, fs, numtaps=401):
    filter = signal.firwin(numtaps, frequency_range, fs=fs, pass_zero=False)
    return signal.convolve(samples, filter, mode='same')
