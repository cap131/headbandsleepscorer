# most functions adapted from tools.py in skjerns/AutoSleepScorer

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import time
from sklearn.metrics import f1_score, accuracy_score
from copy import deepcopy
import os,sys

def show():
    plt.show()

"from AutoSleepScorerDev"
def plot_hypnogram(stages, labels=None, title='', ax1=None, **kwargs):
    if labels is None:
        if np.max(stages)==4:
            print('assuming 0=W, 1=S1, 2=S2, 3=SWS, 4=REM')
            labels = ['W', 'S1', 'S2', 'SWS', 'REM']
        if np.max(stages)==5:
            print('assuming 0=W, 1=S1, 2=S2, 3=S3, 4=S4, 5=SWS')
            labels = ['W', 'S1', 'S2', 'S3', 'S4', 'REM']
        if np.max(stages)==8:
            print('assuming 0=W, 1=S1, 2=S2, 3=S3, 4=S4, 5=SWS')
            labels = ['W', 'S1', 'S2', 'S3', 'S4', 'REM', 'Movement']
    labels_dict = dict(zip(np.arange(len(labels)),labels))

    x = []
    y = []
    for i in np.arange(len(stages)):
        s = stages[i]
        if labels_dict[s]=='W':   p = -0
        if labels_dict[s]=='REM': p = -1
        if labels_dict[s]=='S1':  p = -2
        if labels_dict[s]=='S2':  p = -3
        if labels_dict[s]=='SWS': p = -4
        if labels_dict[s]=='S3': p = -4
        if labels_dict[s]=='S4': p = -5
        if i!=0:
            y.append(p)
            x.append(i-1)
        y.append(p)
        x.append(i)

    x = np.array(x)*30
    y = np.array(y)
    if ax1 is None:
        fig = plt.figure(figsize=[8,2])
        ax1 = fig.add_subplot(111)
    formatter = matplotlib.ticker.FuncFormatter(lambda s, x: time.strftime('%H:%M', time.gmtime(s)))
    ax1.xaxis.set_major_formatter(formatter)
    ax1.plot(x,y, **kwargs)
    plt.yticks([0,-1,-2,-3,-4, -5], ['W','REM', 'S1', 'S2', 'SWS', ''])
    plt.xticks(np.arange(0,x[-1],3600))
    plt.xlabel('Time after recording start')
    plt.ylabel('Sleep Stage')
    plt.title(title)
    plt.tight_layout()

def plot_prediction_hypnogram(y_test, y_pred, save_path=None):
    plt.figure(figsize=[8,3])
    ax = plt.subplot(111)
    plot_hypnogram(y_test, labels=['W', 'S1', 'S2', 'SWS', 'REM'], c ='orangered', title='Ground Truth', ax1=ax)
    plot_hypnogram(y_pred, labels=['W', 'S1', 'S2', 'SWS', 'REM'], c='royalblue', title='Prediction', linewidth=1.9, ax1=ax)
    plt.legend(['Human Scorer','Model Prediction'], loc='lower right')
    if save_path is not None:
        plt.savefig(save_path)

def plot_epochs(data1,data2=[],title='',data1_label='Epochs 1',data2_label='Epochs 2'):
    class Visualizer():
        def __init__(self, data1,data2):
            self.data1 = data1
            self.data2 = data2
            self.pos = 0
            self.scale = 0

        def update(self):
            try:
                self.ax.cla()
                self.ax.plot(self.data1[self.pos,:], label=data1_label, linewidth=1)
                if data2 != []:
                    self.ax.plot(self.data2[self.pos,:], label=data2_label, linewidth=1)
                self.ax.legend()
                # self.ax.set_ylim([-50,200])
                plt.title(title + " Epoch " + str(self.pos))
                self.fig.canvas.draw()

            except Exception as e: print(e)
    def key_event(e):
        v = _vis
        try:
            if e.key == "right":
                v.pos +=1
            elif e.key == "left":
                v.pos -= 1
            elif e.key == "up":
                v.scale += 1
            elif e.key == "down":
                v.scale -= 1
            else:
                print(e.key)

            v.update()

        except Exception as e:print(e)
    global _vis
    _vis = Visualizer(data1,data2)
    _vis.fig = plt.figure()
    _vis.fig.canvas.mpl_connect('key_press_event', key_event)
    _vis.ax = _vis.fig.add_subplot(111)
    _vis.update()

def plot_confusion_matrix(conf_mat, target_names, title='', cmap='Blues', perc=True,figsize=[6,5],cbar=True,save_path=''):
    """Plot Confusion Matrix."""
    figsize = deepcopy(figsize)
    if cbar == False:
        figsize[0] = figsize[0] - 0.6
    c_names = []
    r_names = []
    if len(target_names) != len(conf_mat):
        target_names = [str(i) for  i in np.arange(len(conf_mat))]
    for i, label in enumerate(target_names):
        c_names.append(label + '\n(' + str(int(np.sum(conf_mat[:,i]))) + ')')
        align = len(str(int(np.sum(conf_mat[i,:])))) + 3 - len(label)
        r_names.append('{:{align}}'.format(label, align=align) + '\n(' + str(int(np.sum(conf_mat[i,:]))) + ')')

    cm = conf_mat
    cm = 100* cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    df = pd.DataFrame(data=np.sqrt(cm), columns=c_names, index=r_names)
    if save_path != '':plt.figure(figsize=figsize)
    g  = sns.heatmap(df, annot = cm if perc else conf_mat , fmt=".1f" if perc else ".0f",
                     linewidths=.5, vmin=0, vmax=np.sqrt(100), cmap=cmap, cbar=cbar,annot_kws={"size": 13})
    g.set_title(title)
    if cbar:
        cbar = g.collections[0].colorbar
        cbar.set_ticks(np.sqrt(np.arange(0,100,20)))
        cbar.set_ticklabels(np.arange(0,100,20))
    g.set_ylabel('True sleep stage',fontdict={'fontsize' : 12, 'fontweight':'bold'})
    g.set_xlabel('Predicted sleep stage',fontdict={'fontsize' : 12, 'fontweight':'bold'})
#    plt.tight_layout()
    if save_path!='':
        plt.tight_layout()
        g.figure.savefig(save_path, dpi=192)

# FIXME not adapted yet
def plot_results_per_patient(predictions, targets, groups, title='Results per Patient', fname='results_pp.png'):
    assert len(predictions) ==  len(targets), '{} predictions, {} targets'.format(len(predictions), len(targets))
    IDs = np.unique(groups)
    f1s = []
    accs = []
    if predictions.ndim == 2: predictions = np.argmax(predictions,1)
    if targets.ndim == 2: targets = np.argmax(targets,1)
    statechanges = []
    for ID in IDs:
        y_true = targets [groups==ID]
        y_pred = predictions[groups==ID]
        f1  = f1_score(y_true, y_pred, average='macro')
        acc = accuracy_score(y_true, y_pred)
        f1s.append(f1)
        accs.append(acc)
        statechanges.append(np.sum(0!=y_true-np.roll(y_true,1))-1)
    if fname != '':plt.figure()

    plt.plot(f1s,'go')
    plt.plot(accs,'bo')
    if np.min(f1s) > 0.5:
        plt.ylim([0.5,1])
    plt.legend(['F1', 'Acc'])
    plt.xlabel('Patient')
    plt.ylabel('Score')
    if fname != '':
        title = title + '\nMean Acc: {:.1f} mean F1: {:.1f}'.format(accuracy_score(targets, predictions)*100,f1_score(targets,predictions, average='macro')*100)
    plt.title(title)
#    plt.tight_layout()
    if fname!='':
        plt.savefig(os.path.join('plots', fname), dpi=192)
    return (accs,f1s, statechanges)

# courtesy of Ajitesh Kumar https://vitalflux.com/keras-cnn-image-classification-example/
def plot_keras_model_fit_history(history, save_path='', title=''):
    history_dict = history.history
    loss_values = history_dict['loss']
    val_loss_values = history_dict['val_loss']
    accuracy = history_dict['accuracy']
    val_accuracy = history_dict['val_accuracy']

    epochs = range(1, len(loss_values) + 1)
    fig, ax = plt.subplots(1, 2, figsize=(14, 6))
    #
    # Plot the model accuracy vs Epochs
    #
    ax[0].plot(epochs, accuracy, 'bo', label='Training accuracy')
    ax[0].plot(epochs, val_accuracy, 'b', label='Validation accuracy')
    ax[0].set_title('Training & Validation Accuracy', fontsize=16)
    ax[0].set_xlabel('Epochs', fontsize=16)
    ax[0].set_ylabel('Accuracy', fontsize=16)
    ax[0].legend()
    #
    # Plot the loss vs Epochs
    #
    ax[1].plot(epochs, loss_values, 'bo', label='Training loss')
    ax[1].plot(epochs, val_loss_values, 'b', label='Validation loss')
    ax[1].set_title('Training & Validation Loss', fontsize=16)
    ax[1].set_xlabel('Epochs', fontsize=16)
    ax[1].set_ylabel('Loss', fontsize=16)
    ax[1].legend()
    plt.title(title)

    if save_path != '':
        plt.savefig(save_path, dpi=192)

def plot_mislabled_epochs(X_test, y_pred, y_test, title='Mislabeled Epochs'):
    class MislabelVisualizer():
        def __init__(self, X, y_test, y_pred):
            self.ch1 = X[:,:,0]
            self.ch2 = X[:,:,1]
            self.pos = 0
            self.scale = 0

            self.y_pred = y_pred;
            self.y_test = y_test;
            self.mislabels = np.where(y_pred != y_test)[0]
            self.mislabel_i = 0

            self.labels = ['W', 'S1', 'S2', 'SWS', 'REM']

        def update(self):
            try:
                self.ax.cla()
                self.ax.plot(self.ch1[self.pos,:], label='Channel 1', linewidth=1)
                self.ax.plot(self.ch2[self.pos,:], label='Channel 2', linewidth=1)
                self.ax.legend()
                # self.ax.set_ylim([-50,200])
                print(self.pos)
                true_label = self.labels[int(self.y_test[int(self.pos)])]
                pred_label = self.labels[int(self.y_pred[int(self.pos)])]
                plt.title(title + ", Epoch: " + str(self.pos) + "\nTrue Label: " + true_label + "   Predicted Label: " + pred_label)
                self.fig.canvas.draw()

            except Exception as e: print(e)
    def key_event(e):
        v = _vis_epochs
        try:
            if e.key == "right":
                v.mislabel_i += 1
                if v.mislabel_i == len(v.mislabels):
                    v.mislabel_i = 0
                v.pos = v.mislabels[v.mislabel_i]
            elif e.key == "left":
                v.mislabel_i -= 1
                if v.mislabel_i < 0:
                    v.mislabel_i = 0
                v.pos = v.mislabels[v.mislabel_i]
            elif e.key == "up":
                v.scale += 1
            elif e.key == "down":
                v.scale -= 1
            else:
                print(e.key)

            v.update()

        except Exception as e:print(e)

    assert(len(y_pred) == len(y_test))

    global _vis_epochs
    _vis_epochs = MislabelVisualizer(X_test, y_test, y_pred)
    _vis_epochs.fig = plt.figure()
    _vis_epochs.fig.canvas.mpl_connect('key_press_event', key_event)
    _vis_epochs.ax = _vis_epochs.fig.add_subplot(111)
    _vis_epochs.update()
