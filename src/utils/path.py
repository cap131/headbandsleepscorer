import os
from pathlib import Path
import shutil

def clean_or_create_dir(directory_path):
    if directory_path.exists() and directory_path.is_dir():
        shutil.rmtree(directory_path)
    directory_path.mkdir()

def create_new_iteration_dir(iteration_dir_path, iteration):
    clean_or_create_dir(iteration_dir_path)

    with Path(iteration_dir_path, "iteration.txt").open('w') as file:
        iteration_str = "\n".join("{}\t{}".format(k, v) for k, v in iteration.items())
        file.write(iteration_str)

def write_results(directory, cv_results, n_patients):
    n_patients = n_patients

    with open(os.path.join(directory, 'results.csv'), 'w') as f:
        avg_accuracy = 0.0
        avg_balanced_accuracy = 0.0
        for key in cv_results.keys():
            avg_accuracy = avg_accuracy + cv_results[key]['Accuracy']
            avg_balanced_accuracy = avg_balanced_accuracy + cv_results[key]['Balanced Accuracy']
            f.write("%s, %s\n" % (key, cv_results[key]))

        avg_accuracy = avg_accuracy / n_patients
        avg_balanced_accuracy = avg_balanced_accuracy / n_patients
        f.write("%s, %s\n" % ('Average Accuracy', avg_accuracy))
        f.write("%s, %s\n" % ('Average Balanced Accuracy', avg_balanced_accuracy))

