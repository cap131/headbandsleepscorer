from tensorflow import keras
from sklearn.utils import class_weight
from keras.utils import to_categorical
import numpy as np

class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    ## x -> dataset of dimensions = (patients, num_of_epoch_per_patient, fs*epoch_len, num_channels)
    def __init__(self, x, y, batch_size=32, fs=100, seconds=30, n_channels=2,
                 seq_len=10, n_classes=5, shuffle=True):
        'Initialization'
        assert(seq_len % 2 == 1)
        self.seq_len = seq_len
        if seq_len == 1:
            self.dim = (fs*seconds, n_channels)
        else:
            self.dim = (seq_len, fs*seconds, n_channels)
        self.batch_size = batch_size
        self.data = x
        self.labels = y
        self.n_classes = n_classes
        self.shuffle = shuffle

        for patient_i in range(len(y)):
            cat = to_categorical(y[patient_i])
            self.labels[patient_i] = cat

        ## (1,0) (1,1) (1,2) ....
        ## (2,0) (2,1) (2,2) ....
        ## (3,0) (3,1) (3,2) ....

        ## (1,0) (1,1) (1,2) ... (2,0)
        self.indexes = np.array([(i, j) for i in range(len(x))
                                             for j in range(seq_len//2, len(x[i]) - seq_len//2)
                                    ])
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return len(self.indexes) // self.batch_size

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        sub_idx = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Generate data
        X, y = self.__data_generation(sub_idx)

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, sub_idx):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        seq_len = self.seq_len
        if seq_len == 1:
            batch_data = np.array([self.data[p_num][p_epoch] for p_num,p_epoch in sub_idx])
        else:
            batch_data = np.array([self.data[p_num][p_epoch-self.seq_len//2 : p_epoch+seq_len//2+1, :, :] for p_num, p_epoch in sub_idx])

        batch_label = np.array([self.labels[p_num][p_epoch] for p_num,p_epoch in sub_idx])

        return batch_data, batch_label

def split_and_weight(X, y, leaveout):
    X_train, y_train = np.delete(X, leaveout), np.delete(y, leaveout)
    X_test, y_test = [X[leaveout]], [y[leaveout]]

    class_w = {}
    class_w = class_weight.compute_class_weight("balanced", y=y_test[0], classes=[0,1,2,3,4])
    class_w = {i : class_w[i] for i in range(len(class_w))}

    return (X_train, y_train), (X_test, y_test), class_w

def create_data_generators(X, y, leaveout, seq_len=1, fs=100):
    train, test, class_w = split_and_weight(X, y, leaveout)
    train_gen = DataGenerator(train[0], train[1], seq_len=seq_len, fs=fs)
    test_gen = DataGenerator(test[0], test[1], seq_len=seq_len, fs=fs, shuffle=False)
    return train_gen, test_gen, class_w
