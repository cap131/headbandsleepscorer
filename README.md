# Auto Sleep Scorer for Headband Data

## Intro

This repository was used to generate the deep learning results in the "A Deep Learning Strategy for Automatic Sleep Staging Based on Two-Channel EEG Headband Data" paper. It contains a component based framework which runs a data-processing pipeline, and then creates a machine learning model and trains and evaluates it.

The system it is currently configured to run achieves a mean accuracy of 74.01% (standard deviation 10.32%) when it is trained and tested on our headband data collected from 12 patients overnight and using expert PSG scoring results as comparison. These patients were simultaneously monitored with an attended PSG level 1 setup, as well as a Cognionics 2-channel EEG headband. The overnight PSG was scored by a Senior Polysomnographic Technologist and interpreted by a board-certified sleep physician using standard AASM criteria based on 6 EEG channels, 2 EOG channels, and 1 EMG channel.

## Setup

1. Install python packages `pip install -r requirements.txt`
2. Place your EEG data in the `data` directory

## How to train and evaluate with your data

1. Create a data loader for your data in the `stages/load_data.py` file.
2. Import your data loader in the `main.py` file (or Main.ipynb if you prefer jupyter notebook), and specify it in the `dataset_loader` field in the `iteration` dictionary.
3. Change the `iteration_name` to a custom iteration name.
4. Adjust hyper-parameters or components in the iteration dictionary to your liking. It is currently configured to be the best performing model & hyper-parameters we present in our paper. Be sure to read the `NOTE` comments if there are issues running the model.
5. Run the main.py file.
6. The trained model, dictionary, and evaluation results will be generated under `iterations/` under the interation name specified.
